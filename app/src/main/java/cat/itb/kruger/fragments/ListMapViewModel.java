package cat.itb.kruger.fragments;

import android.app.Application;
import android.net.Uri;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.ListResult;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.List;

import cat.itb.kruger.model.Animal;
import io.grpc.Metadata;

public class ListMapViewModel extends AndroidViewModel {

    MutableLiveData<List<Animal>> animals = new MutableLiveData<>();
    List<Animal> listAux = new ArrayList<>();

    Application application;
    StorageReference storage;

    public ListMapViewModel(@NonNull Application application) {
        super(application);
        this.application = application;
        this.storage = FirebaseStorage.getInstance().getReference();
        animals = getListAll();
    }

    /**
     * Método que recoge todos los datos introducidos por los usuarios en el storage de Firebase
     * @return Lista de animales
     */
    public MutableLiveData<List<Animal>> getListAll() {
        StorageReference storage = FirebaseStorage.getInstance().getReference();

        storage.child("animals/").listAll()
                .addOnSuccessListener(new OnSuccessListener<ListResult>() {
                    @Override
                    public void onSuccess(ListResult listResult) {
                        for (StorageReference item : listResult.getItems()) {
                            item.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    Animal animal = new Animal();
                                    animal.setUriImg(item);
                                    item.getMetadata().addOnSuccessListener(new OnSuccessListener<StorageMetadata>() {
                                        @Override
                                        public void onSuccess(StorageMetadata storageMetadata) {
                                            StorageMetadata metada = storageMetadata;

                                            animal.setUbic(metada.getCustomMetadata("Location"));
                                            animal.setRace(metada.getCustomMetadata("Race"));
                                            animal.setDate(metada.getCustomMetadata("Date"));
                                            animal.setLat(Double.parseDouble(metada.getCustomMetadata("Latitude")));
                                            animal.setLng(Double.parseDouble(metada.getCustomMetadata("Longitude")));
                                            listAux.add(animal);
                                            animals.postValue(listAux);

                                        }
                                    }).addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            Toast.makeText(application.getApplicationContext(), "HA FALLAT METADATA", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Toast.makeText(application.getApplicationContext(), "ha fallat get download ", Toast.LENGTH_SHORT).show();
                                }
                            });

                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(application.getApplicationContext(), "HA FALLAT", Toast.LENGTH_SHORT).show();
            }
        });

        return animals;
    }


    public LiveData<List<Animal>> getAnimals() {
        return animals;
    }


}
