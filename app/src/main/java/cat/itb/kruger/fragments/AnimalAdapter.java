package cat.itb.kruger.fragments;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.github.chrisbanes.photoview.PhotoView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cat.itb.kruger.R;
import cat.itb.kruger.model.Animal;

/**
 * Clase que permite crear un RecyclerView introduciéndole los datos
 */
public class AnimalAdapter extends RecyclerView.Adapter<AnimalAdapter.AnimalViewHolder>{

    private Context myContext;
    private List<Animal> myAnimals;

    public AnimalAdapter(Context context, List<Animal> animals) {
        this.myContext = context;
        this.myAnimals = animals;

    }

    @NonNull
    @Override
    public AnimalAdapter.AnimalViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(myContext).inflate(R.layout.list_item,parent,false);
        return new AnimalViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AnimalAdapter.AnimalViewHolder holder, int position) {

        Animal animal= myAnimals.get(position);
        Glide.with(myContext)
                .load(animal.getUriImg())
                .into(holder.ivAnimal);
        holder.tvRace.setText(animal.getRace());
        holder.tvHour.setText(animal.getDate());
        holder.tvLocal.setText(animal.getUbic());
    }

    @Override
    public int getItemCount() {
        int size=0;
        if(myAnimals !=null){
            size= myAnimals.size();
        }
        return size;
    }

    public void setMyAnimals(List<Animal> myAnimals) {
        this.myAnimals = myAnimals;
        notifyDataSetChanged();
    }

    public class AnimalViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.ivAnimal)
        PhotoView ivAnimal;

        @BindView(R.id.tvRace)
        TextView tvRace;

        @BindView(R.id.tvDate)
        TextView tvHour;

        @BindView(R.id.tvLocalList)
        TextView tvLocal;

        public AnimalViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
