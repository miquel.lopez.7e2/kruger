package cat.itb.kruger.fragments;

import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cat.itb.kruger.R;
import cat.itb.kruger.model.Animal;

public class MapFragment extends Fragment implements OnMapReadyCallback{

    GoogleMap mGoogleMap;

    ListMapViewModel mViewModel;

    @BindView(R.id.mapView)
    MapView mapView;

    View mView;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        mView= inflater.inflate(R.layout.map_fragment, container, false);
        return mView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        if(mapView!=null){
            mapView.onCreate(null);
            mapView.onResume();
            mapView.getMapAsync(this);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(ListMapViewModel.class);

        LiveData<List<Animal>> animals = mViewModel.getAnimals();
        animals.observe(this, this::onAnimalsChanged);


    }

    private void onAnimalsChanged(List<Animal> animals){
        for(Animal animal: animals){
            addMarkers(mGoogleMap,animal);
        }
    }

    /**
     * Metodo que inicializa el mapa y añade la localización del usuario en él
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        MapsInitializer.initialize(getContext());
        mGoogleMap= googleMap;
        mGoogleMap.setMyLocationEnabled(true);
    }

    /**
     * Método para añadir marcadores de posición en el mapa
     * @param googleMap
     * @param animal
     */
    public void addMarkers(GoogleMap googleMap, Animal animal){

        Glide.with(getContext())
                .asBitmap()
                .load(animal.getUriImg())
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                        Bitmap thumbnail = ThumbnailUtils.extractThumbnail(resource,100,100);
                        googleMap.addMarker(new MarkerOptions()
                                .position(new LatLng(animal.getLat(),animal.getLng()))
                                .title(animal.getRace())
                                .snippet(animal.getDate())
                                .icon(BitmapDescriptorFactory.fromBitmap(thumbnail)));
                    }
                });
    }

    /**
     * Método para navegar entre fragmentos, de Map a ListAnimals
     */
    @OnClick(R.id.listFAB)
    public void onListClicked(){
        Navigation.findNavController(Objects.requireNonNull(getView())).navigate(R.id.action_mapFragment_to_listAnimalsFragment);
    }

    /**
     * Método para navegar entre fragmentos, de Map a AddAnimal
     */
    @OnClick(R.id.addFAB)
    public void onAddClicked(){
        Navigation.findNavController(Objects.requireNonNull(getView())).navigate(R.id.action_mapFragment_to_addAnimalFragment);
    }

}
