package cat.itb.kruger.fragments;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cat.itb.kruger.MainActivity;
import cat.itb.kruger.R;

import static android.app.Activity.RESULT_OK;


public class AddAnimalFragment extends Fragment {


    /*
     * VARIABLES
     */

    private String currentPhotoPath = null;
    private double latitud;
    private double longitud;
    private String location;
    private Uri photoURI;
    private String dateAux;
    private AddAnimalViewModel mViewModel;
    static final int REQUEST_IMAGE_CAPTURE = 1;
    ProgressDialog progress;

    /*
     * BINDVIEWS
     */

    @BindView(R.id.imgViewTakePicture)
    ImageButton picture;

    @BindView(R.id.edRace)
    EditText race;

    @BindView(R.id.tvHourDetail)
    TextView hour;

    @BindView(R.id.tvLocalizationDetail)
    TextView localization;

    /**
     * ----------------------------------------------------------------------------------------------------------------
     * ----------------------------------------------------------------------------------------------------------------
     */

    public static AddAnimalFragment newInstance() {
        return new AddAnimalFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.add_animal_fragment, container, false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        locationStart();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(AddAnimalViewModel.class);


    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    /**
     * Metodo que permite hacer la foto y la guarda en el dispositivo
     */

    @OnClick(R.id.imgViewTakePicture)
    public void takingPicture() {
        takePictureIntent();
    }

    /**
     * Metodo que permite subir la foto a FireStore
     */

    @OnClick(R.id.btnUpload)
    public void uploadImage() {

        if (currentPhotoPath == null) {
            Toast.makeText(getActivity(), "No has seleccionat cap imatge!",
                    Toast.LENGTH_LONG).show();
        } else {
            mViewModel.uploadFile(dateAux, race, latitud, longitud, location, currentPhotoPath);
            mViewModel.getUploaded().observe(this, this::onUploadChanged);
            mViewModel.getUploading().observe(this, this::onUploadingChange);

        }
    }

    /**
     * Método que muestra un diálogo mientras se sube la imagen a Firestore
     */
    private void onUploadingChange(Boolean isUploading) {

        if (isUploading) {
            progress = ProgressDialog.show(getContext(), "Pujant foto a firestore", "Espera un moment siusplau...");
        } else {
            progress.dismiss();
        }
    }

    /**
     *Método que muestra un Toast cuando la imagen se ha subido a Firestore
     * @param isUploaded boolean que indica si está o no subida
     */
    private void onUploadChanged(Boolean isUploaded) {

        if (isUploaded) {
            progress.dismiss();
            Navigation.findNavController(getView()).navigate(R.id.action_addAnimalFragment_to_listAnimalsFragment);
            Toast.makeText(getActivity(), "Imatge pujada a firestore!",
                    Toast.LENGTH_LONG).show();
        }
    }

    private void takePictureIntent() {

        /**
         * INTENT
         */

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        /**
         * CREAR ARCHIVO PARA GUARDAR LA IMAGEN EN EL DISPOSITIVO
         */

        File photoFile = null;
        try {
            photoFile = createImageFile();
        } catch (IOException ex) {
            Toast.makeText(getActivity(), "No s'ha pogut crear l'arxiu. Torna a intentar-ho!",
                    Toast.LENGTH_LONG).show();
        }

        if (photoFile != null) {
            photoURI = FileProvider.getUriForFile(getContext(),
                    "cat.itb.kruger.fileprovider",
                    photoFile);
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        hour.setText(dateAux);

        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bitmap bitmap = BitmapFactory.decodeFile(currentPhotoPath);
            picture.setImageBitmap(bitmap);


        } else {
            Toast.makeText(getActivity(), "No has seleccionat cap imatge!",
                    Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Metodo que permite crear el archivo para guardar la imagen en el dispositivo
     *
     * @return Archivo
     * @throws IOException
     */

    private File createImageFile() throws IOException {

        String imageFileName;
        dateAux = new SimpleDateFormat("dd/MM/yyyy  HH:mm").format(new Date());
        String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());

        File storageDir = getContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);

        imageFileName = "JPEG-" + timeStamp + " ";

        File image = File.createTempFile(
                imageFileName,
                ".jpg",
                storageDir
        );

        currentPhotoPath = image.getAbsolutePath();
        galleryAddPic(currentPhotoPath);
        return image;
    }


    /**
     * Metodo que permite añadir la foto al almacenamiento del dispositivo
     *
     * @param filePath Path de la imagen
     */

    private void galleryAddPic(String filePath) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(filePath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        getContext().sendBroadcast(mediaScanIntent);
    }

    /**
     * Metodo que inicia el servicio de localización en el dispositivo
     */
    public void locationStart() {

        LocationManager mlocManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        Localizacion local = new Localizacion();
        local.setActivity((MainActivity) getActivity());
        final boolean gpsEnabled = mlocManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (!gpsEnabled) {
            Intent settingsIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(settingsIntent);
        }
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION,}, 1000);
            return;
        }
        mlocManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, local);
        mlocManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, local);
    }


    /**
     * Método que permite, mediante una localización, dar con la ubicación del dispositivo
     */
    public void setLocation(Location loc) {

        if (loc.getLatitude() != 0.0 && loc.getLongitude() != 0.0) {
            try {
                Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
                List<Address> list = geocoder.getFromLocation(
                        loc.getLatitude(), loc.getLongitude(), 1);
                if (!list.isEmpty()) {
                    Address dirCalle = list.get(0);
                    location = dirCalle.getLocality() + ", " + dirCalle.getSubAdminArea() + ", " + dirCalle.getCountryName();
                    localization.setText(location);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    /**
     * Clase que implementa la interfaz LocationListener para controlar los cambios de ubicación en tiempo real
     */
    public class Localizacion implements LocationListener {

        MainActivity activity;

        public void setActivity(MainActivity activity) {
            this.activity = activity;
        }

        @Override
        public void onLocationChanged(Location loc) {

            latitud = loc.getLatitude();
            longitud = loc.getLongitude();
            setLocation(loc);
        }

        @Override
        public void onProviderDisabled(String provider) {

            Toast.makeText(getActivity(), "GPS desactivat!",
                    Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onProviderEnabled(String provider) {

            Toast.makeText(getActivity(), "GPS activat!",
                    Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            switch (status) {
                case LocationProvider.AVAILABLE:
                    Log.d("debug", "LocationProvider.AVAILABLE");
                    break;
                case LocationProvider.OUT_OF_SERVICE:
                    Log.d("debug", "LocationProvider.OUT_OF_SERVICE");
                    break;
                case LocationProvider.TEMPORARILY_UNAVAILABLE:
                    Log.d("debug", "LocationProvider.TEMPORARILY_UNAVAILABLE");
                    break;
            }
        }
    }

}
