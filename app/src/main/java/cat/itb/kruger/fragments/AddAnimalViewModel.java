package cat.itb.kruger.fragments;

import android.app.Application;
import android.net.Uri;
import android.widget.EditText;
import android.widget.Toast;


import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;


import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;


public class AddAnimalViewModel extends AndroidViewModel {


    MutableLiveData<Boolean> uploaded = new MutableLiveData<>(false);
    MutableLiveData<Boolean> uploading = new MutableLiveData<>(false);

    Application application;

    public AddAnimalViewModel(@NonNull Application application) {
        super(application);
        this.application = application;
    }

    /**
     * Método que sube una foto a Firestore incluyendo sus metadatos
     */
    public void uploadFile(String dateAux, EditText race, double latitud, double longitud, String location, String currentPhotoPath) {

        uploading.setValue(true);

        /**
         * INSTANCIA A FIRESTORE
         */

        StorageReference storageReference = FirebaseStorage.getInstance().getReference();

        /**
         * COGER LA FOTO
         */

        Uri file = Uri.fromFile(new File(currentPhotoPath));
        StorageReference photoReference = storageReference.child("animals/" + file.getLastPathSegment());

        /**
         * AÑADIRLE METADATOS
         */

        StorageMetadata metadataImage = new StorageMetadata.
                Builder().
                setCustomMetadata("Race", race.getText().toString()).
                setCustomMetadata("Latitude", String.valueOf(latitud)).
                setCustomMetadata("Longitude", String.valueOf(longitud)).
                setCustomMetadata("Location", location).
                setCustomMetadata("Date", dateAux).
                build();

        /**
         * SUBIR LA FOTO
         */

        UploadTask uploadImage = photoReference.putFile(file, metadataImage);

        uploadImage.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

                uploading.postValue(false);
                Toast.makeText(application, "Hi ha agut un error pujant les fotos",
                        Toast.LENGTH_LONG).show();

            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                uploaded.setValue(true);
                uploading.setValue(false);

            }
        });

    }


    public MutableLiveData<Boolean> getUploaded() {
        return uploaded;
    }

    public MutableLiveData<Boolean> getUploading() {
        return uploading;
    }

}
