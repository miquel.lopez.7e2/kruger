package cat.itb.kruger.fragments;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProviders;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;


import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cat.itb.kruger.R;
import cat.itb.kruger.model.Animal;

public class ListAnimalsFragment extends Fragment {

    private ListMapViewModel mViewModel;
    private List<Animal> animals = new ArrayList<>();

    @BindView(R.id.animalRV)
    RecyclerView animalRV;

    private ScaleGestureDetector scaleGestureDetector;
    private float mScaleFactor= 1.0f;

    AnimalAdapter adapter;

    public static ListAnimalsFragment newInstance() {
        return new ListAnimalsFragment();
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /**
         * Pide los permisos al usuario en caso de no tener la aplicación
         */

        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        return inflater.inflate(R.layout.list_animals_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(ListMapViewModel.class);

        //RECYCLERVIEW

        animalRV.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this.getActivity());
        animalRV.setLayoutManager(layoutManager);

        //ADAPTER

        adapter = new AnimalAdapter(getContext(), animals);
        animalRV.setAdapter(adapter);

        LiveData<List<Animal>> animals = mViewModel.getAnimals();
        animals.observe(this, this::onAnimalsChanged);

    }

    /**
     * Método que controla los canvios de la base de datos con respecto a los mostrados, en caso de estar vacía la lista, muestra un Toast que lo indica
     * @param animals Lista de los animales a disponer
     */
    private void onAnimalsChanged(List<Animal> animals) {
        adapter.setMyAnimals(animals);
        if (animals.isEmpty()) {
            Toast.makeText(getContext(), "Llista buida", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Método para navegar entre fragmentos, de ListAnimals a AddAnimal
     */
    @OnClick(R.id.AddFAB)
    public void onAddClicked() {
        Navigation.findNavController(Objects.requireNonNull(getView())).navigate(R.id.action_listAnimalsFragment_to_addAnimalFragment);
    }

    /**
     * Método para navegar entre fragmentos, de ListAnimals a Map
     */
    @OnClick(R.id.mapFAB)
    public void onMapClicked() {

        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            Navigation.findNavController(Objects.requireNonNull(getView())).navigate(R.id.action_listAnimalsFragment_to_mapFragment);
        } else {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        }

    }




}
