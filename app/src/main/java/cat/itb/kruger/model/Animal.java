package cat.itb.kruger.model;

import com.google.firebase.storage.StorageReference;

public class Animal {
    private StorageReference uriImg;
    private String race;
    private String date;
    private String ubic;
    private double lat;
    private double lng;



    public Animal() {

    }

    public StorageReference getUriImg() {
        return uriImg;
    }

    public void setUriImg(StorageReference uriImg) {
        this.uriImg = uriImg;
    }

    public String getRace() {
        return race;
    }

    public void setRace(String race) {
        this.race = race;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getUbic() {
        return ubic;
    }

    public void setUbic(String ubic) {
        this.ubic = ubic;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }
}
