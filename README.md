Aquesta aplicació ha sigut creada per Andrea Valcàrcel i Miquel López

L'aplicació consta dels següents apartats mínims:

- Realitzar imatges amb la càmera.
- Guardar les imatges tant localment com pujar-les a FireStore. 
- Mostrar imatges de FireStore. 
- Mostrar ubicacions en un mapa.

APARTATS EXTRA:

- Mostrar la ubicació de la foto per municipi, provincia i país (Ex: Sant Cugat, Barcelona, Espanya)  mitjançant la clase Geocoder i la latitud i longitud. 
- Les imatges que es descarreguen del FireStore i es visualitzen al fragment del llistat de fotos, es poden fer zoom per veure-les millor.


LINK DEL FIGMA ON ES VEU EL DISSENY INICIAL:

https://www.figma.com/file/ywEwWqdViyxrPLUbzbbVRK/Kruger?node-id=11%3A5867